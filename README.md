﻿TLS fixer
===================================
Fix for Windows NT 5.x loader to correctly dynamic load/unload DLLs with TLS data.

Status
-----------------------------------
The fixer is tested only with few versions of Windows XP SP3 system DLL-s and may fail with others as it relies on internal system DLL-s structure.

So it's strongly recommended to contact the author if you want to use it seriously and you aren't a masochist.

Usage
-----------------------------------
Just load fixer DLL (which one can get in *Downloads* section) into the program to fix the loader. If you have no access to the program sources use a DLL injecting util (e.g. `dllinj` from my *hooking* project).

License
-----------------------------------
The project is licensed under the terms of the [Boost Software License, Version 1.0](http://boost.org/LICENSE_1_0.txt).
