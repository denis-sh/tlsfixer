/** Helper functions x86 instructions manipulation.

Almost copy of $(D hooking.x86.utils).

Copyright: Denis Shelomovskij 2012-2013

License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module tlsfixer.x86utils;


pure nothrow:

inout(void)* getAbsoluteTarget(inout(void)* address)
{
	return *cast(inout(void)**) address;
}

inout(void)* getRelativeTarget(inout(void)* address, in ubyte addressSize = 4)
in { assert(addressSize == 1 || addressSize == 4); }
body
{
	return address + addressSize + 
		(addressSize == 1 ? *cast(byte*) address :
		*cast(int*) address);
}

/// Find a code sequence and return the address after the sequence
inout(void)* findCodeSequence(inout(void)* startAddress, in size_t len, in string pattern)
{
	import core.stdc.string: memcmp;

	if(startAddress)
		foreach(p; 0 .. len)
			if(memcmp(startAddress + p, pattern.ptr, pattern.length) == 0)
				return startAddress + p + pattern.length;
	return null;
}

/// Find a code sequence and return the (relative) address that follows
inout(void)* findCodeReference(inout(void)* startAddress, in size_t len, in string pattern, in bool relative, in ubyte addressSize = 4)
in { assert(relative || addressSize == 4); }
body
{
	if(auto p = findCodeSequence(startAddress, len, pattern))
		return relative ? getRelativeTarget(p, addressSize) : getAbsoluteTarget(p);
	return null;
}
